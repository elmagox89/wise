import random
import string

from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from datetime import datetime

from clients import models as clients_models


def random_string(length=10):
    # Create a random string of length length
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(length))


def create_User(**kwargs):
    defaults = {
        "username": "%s_username" % random_string(5),
        "email": "%s_username@tempurl.com" % random_string(5),
    }
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_AbstractUser(**kwargs):
    defaults = {
        "username": "%s_username" % random_string(5),
        "email": "%s_username@tempurl.com" % random_string(5),
    }
    defaults.update(**kwargs)
    return AbstractUser.objects.create(**defaults)


def create_AbstractBaseUser(**kwargs):
    defaults = {
        "username": "%s_username" % random_string(5),
        "email": "%s_username@tempurl.com" % random_string(5),
    }
    defaults.update(**kwargs)
    return AbstractBaseUser.objects.create(**defaults)


def create_Group(**kwargs):
    defaults = {
        "name": "%s_group" % random_string(5),
    }
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_ContentType(**kwargs):
    defaults = {
    }
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_clients_BureauScores(**kwargs):
    defaults = {}
    defaults["experian_score"] = ""
    defaults["equifax_score"] = ""
    defaults["transunion_score"] = ""
    defaults.update(**kwargs)
    return clients_models.BureauScores.objects.create(**defaults)
def create_clients_Client(**kwargs):
    defaults = {}
    defaults["billing_address"] = ""
    defaults["employer_name"] = ""
    defaults["social_security_number"] = ""
    defaults["email"] = ""
    defaults["birthdate"] = datetime.now()
    defaults["residential_address"] = ""
    defaults["phone_number"] = ""
    defaults["name"] = ""
    defaults.update(**kwargs)
    return clients_models.Client.objects.create(**defaults)
def create_clients_PDFInformation(**kwargs):
    defaults = {}
    defaults["file_path"] = ""
    defaults["type"] = ""
    defaults["file_name"] = ""
    defaults["file_size"] = ""
    if "client_id" not in kwargs:
        defaults["client_id"] = create_clients_Client()
    defaults.update(**kwargs)
    return clients_models.PDFInformation.objects.create(**defaults)
def create_clients_RevolvingAccountsDetails(**kwargs):
    defaults = {}
    defaults["account_rating"] = ""
    defaults["last_payment"] = datetime.now()
    defaults["payment_status"] = ""
    defaults["creditor_remarks"] = ""
    defaults["date_opened"] = datetime.now()
    defaults["high_balance"] = ""
    defaults["closed_date"] = datetime.now()
    defaults["date_of_last_activity"] = datetime.now()
    defaults["account_number"] = ""
    defaults["date_reported"] = datetime.now()
    defaults["balance_owed"] = ""
    defaults["account_status"] = ""
    defaults["payment_amount"] = ""
    defaults["creditor_type"] = ""
    defaults["dispute_status"] = ""
    defaults["last_verified"] = ""
    defaults["account_description"] = ""
    defaults["term_length"] = ""
    if "pdf_data_id" not in kwargs:
        defaults["pdf_data_id"] = create_clients_PDFInformation()
    defaults.update(**kwargs)
    return clients_models.RevolvingAccountsDetails.objects.create(**defaults)
