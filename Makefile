# Definir el nombre del servicio de Django en tu docker-compose.yml
SERVICE=web

# Construir la imagen Docker
build:
	docker-compose build

# Iniciar los contenedores
up:
	docker-compose up

reup: cleanm mm clean build up dpdfs
# Detener los contenedores
down:
	docker-compose down

logs:
	docker-compose logs -f --tail 50 $(SERVICE)

# Crear migraciones
mm:
	docker-compose exec $(SERVICE) python manage.py makemigrations

# Ejecutar migraciones
migrate:
	docker-compose exec $(SERVICE) python manage.py migrate

# Crear un superusuario
createsuperuser:
	docker-compose exec $(SERVICE) python manage.py createsuperuser

# Recoger archivos estáticos
collectstatic:
	docker-compose exec $(SERVICE) python manage.py collectstatic --noinput

# Reiniciar los contenedores
restart:
	docker-compose restart

# Ejecutar pruebas
test:
	docker-compose exec $(SERVICE) python manage.py test

# Acceder al shell de Django
shell:
	docker-compose exec $(SERVICE) python manage.py shell

# Acceder a la consola bash del contenedor
bash:
	docker-compose exec $(SERVICE) /bin/bash

clean:
	docker-compose down -v

dpdfs:
	rm -rf upload/pdfs/*

cleanm:
	@echo "Eliminando archivos de migración (excepto __init__.py)..."
	@find . -path "*/migrations/*.py" ! -name "__init__.py" -delete
	@find . -path "*/migrations/*.pyc" -delete
	@echo "Archivos de migración eliminados."

de:
	rm "error_logs.json"


.PHONY: build up down logs migrate createsuperuser collectstatic restart test shell bash clean-volumes
