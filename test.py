import fitz
import json
import re

# Constants
TRANSUNION_X = 174.3280792236328
EXPERIAN_X = 308.6957702636719
EQUIFAX_X = 443.0634460449219

revolving_accounts = []

PDF_PATH = 'juan.pdf'
#PDF_PATH = 'judy.pdf'

TABLE_ROW_IDENTIFIERS = [
    "Account #", "High Balance:", "Last Verified:", "Date of Last Activity:",
    "Date Reported:", "Date Opened:", "Balance Owed:", "Closed Date:",
    "Account Rating:", "Account Description:", "Dispute Status:", "Creditor Type:",
    "Account Status:", "Payment Status:", "Creditor Remarks:", "Payment Amount:",
    "Last Payment:", "Term Length:", "Past Due Amount:", "Account Type:",
    "Payment Frequency:", "Credit Limit:"
]
EXCLUDE_SIZES = [
    6.727727890014648, 7.849016189575195, 18.875015258789062, 14.158130645751953,
    10.465354919433594, 14.950506210327148
]
EXCLUDE_TEXTS = [
    "Partnered", "with", "OK", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct", "Nov", "Dec", "'22", "'23", "'24", "Show"
]

def to_snake_case(text):
    text = re.sub(r"[^a-zA-Z0-9]", " ", text)
    text = text.lower()
    text = re.sub(r"\s+", "_", text)
    return text.strip("_")

# Improved function names and added documentation
def should_exclude_span_by_text(span):
    """Returns True if the span's text should be excluded based on predefined criteria."""
    return span['text'] in EXCLUDE_TEXTS

def should_exclude_span_by_size(span):
    """Returns True if the span's size should be excluded based on predefined criteria."""
    return span['size'] in EXCLUDE_SIZES

def extract_pdf_text_details(pdf_path):
    """Extracts text details from a PDF while filtering out unwanted text and sizes."""
    pdf_text_details = []

    with fitz.open(pdf_path) as pdf:
        for page in pdf:
            blocks = page.get_text("dict")["blocks"]
            for block in blocks:
                if block["type"] == 0:
                    for line in block["lines"]:
                        for span in line["spans"]:
                            if not should_exclude_span_by_size(span) and not should_exclude_span_by_text(span):
                                pdf_text_details.append({
                                    'text': span['text'],
                                    'size': span['size'],
                                    'bbox': span['bbox'],
                                })

    return pdf_text_details


def group_texts_near_x_coordinate(pdf_path, target_x, tolerance=0.5, keyword=None, expected_group_size=22):
    grouped_texts = []
    current_group = []

    with fitz.open(pdf_path) as pdf:
        for page in pdf:
            blocks = page.get_text("dict")["blocks"]
            for block in blocks:
                if block["type"] != 0:
                    continue

                for line in block["lines"]:
                    for span in line["spans"]:
                        x0 = span['bbox'][0]
                        if should_exclude_span_by_size(span) or should_exclude_span_by_text(span) or not (
                                target_x - tolerance <= x0 <= target_x + tolerance):
                            continue

                        if span['size'] == 11.960405349731445:
                            revolving_accounts.append({span['text']})

                        text = span['text']
                        if keyword and text == keyword and current_group:
                            extra_fields_count = len(current_group) - (expected_group_size + 1)  # +1 por el título
                            if 1 <= extra_fields_count <= 3:  # Si hay entre 23 y 25 campos en total
                                merge_start_index = 15  # Comenzar la fusión en la posición 16
                                merge_end_index = 15 + extra_fields_count  # Determinar hasta qué campo fusionar
                                merged_fields = ' '.join(current_group[merge_start_index:merge_end_index + 1])
                                current_group = current_group[:merge_start_index] + [merged_fields] + current_group[merge_end_index + 1:]

                            if len(current_group) == expected_group_size + 1:  # Asegurar el tamaño correcto después de la fusión
                                grouped_texts.append(current_group)
                            current_group = []
                        current_group.append(text)

    if current_group:
        extra_fields_count = len(current_group) - (expected_group_size + 1)
        if 1 <= extra_fields_count <= 3:
            merge_start_index = 15
            merge_end_index = 15 + extra_fields_count
            merged_fields = ' '.join(current_group[merge_start_index:merge_end_index + 1])
            current_group = current_group[:merge_start_index] + [merged_fields] + current_group[merge_end_index + 1:]
        if len(current_group) == expected_group_size + 1:
            grouped_texts.append(current_group)

    return grouped_texts, revolving_accounts


def associate_credit_report_data_with_identifiers(credit_report_data, identifiers):
    """Asocia cada lista de valores de reporte de crédito con sus identificadores correspondientes en snake case."""
    associated_data = []
    # Convertir identificadores a snake case
    identifiers_snake_case = [to_snake_case(identifier) for identifier in identifiers]
    for report in credit_report_data:
        # Considerando el primer elemento como el título y no un dato a asociar
        report_dict = {"Bureau": report[0]}
        report_values = report[1:]  # Excluyendo el título del reporte de los datos a asociar

        if len(report_values) == len(identifiers_snake_case):
            report_dict.update({identifiers_snake_case[i]: report_values[i] for i in range(len(identifiers_snake_case))})
        else:
            print(
                f"Advertencia: Un reporte tiene una cantidad de elementos ({len(report_values)}) diferente a la esperada ({len(identifiers_snake_case)}).")
        associated_data.append(report_dict)
    return associated_data


# Extraer y procesar la información del PDF
pdf_text_details = extract_pdf_text_details(PDF_PATH)

column_transunion_groups = group_texts_near_x_coordinate(PDF_PATH, TRANSUNION_X, keyword="Transunion")
column_experian_groups = group_texts_near_x_coordinate(PDF_PATH, EXPERIAN_X, keyword="Experian")
column_equifax_groups = group_texts_near_x_coordinate(PDF_PATH, EQUIFAX_X, keyword="Equifax")

# Asociar los datos extraídos con sus identificadores
associated_transunion_data = associate_credit_report_data_with_identifiers(column_transunion_groups, TABLE_ROW_IDENTIFIERS)
associated_experian_data = associate_credit_report_data_with_identifiers(column_experian_groups, TABLE_ROW_IDENTIFIERS)
associated_equifax_data = associate_credit_report_data_with_identifiers(column_equifax_groups, TABLE_ROW_IDENTIFIERS)

# Guardar los datos asociados en archivos JSON
# with open('_data_transunion_associated.json', 'w') as tu_file:
#     json.dump(associated_transunion_data, tu_file, indent=4)
#
# with open('_data_experian_associated_data.json', 'w') as ex_file:
#     json.dump(associated_experian_data, ex_file, indent=4)
#
# with open('_data_equifax_associated_data.json', 'w') as eq_file:
#     json.dump(associated_equifax_data, eq_file, indent=4)
