from django.contrib import admin
from django import forms

from . import models


class BureauScoresAdminForm(forms.ModelForm):

    class Meta:
        model = models.BureauScores
        fields = "__all__"


class BureauScoresAdmin(admin.ModelAdmin):
    form = BureauScoresAdminForm
    list_display = [
        "id",
        "experian_score",
        "equifax_score",
        "transunion_score",
        "created",
        "last_updated",
    ]
    readonly_fields = [
        "id",
        "experian_score",
        "equifax_score",
        "transunion_score",
        "created",
        "last_updated",
    ]


class ClientAdminForm(forms.ModelForm):

    class Meta:
        model = models.Client
        fields = "__all__"


class ClientAdmin(admin.ModelAdmin):
    form = ClientAdminForm
    list_display = [
        "created",
        "billing_address",
        "employer_name",
        "social_security_number",
        "last_updated",
        "email",
        "birthdate",
        "residential_address",
        "phone_number",
        "name",
        "id",
    ]
    readonly_fields = [
        "created",
        "billing_address",
        "employer_name",
        "social_security_number",
        "last_updated",
        "email",
        "birthdate",
        "residential_address",
        "phone_number",
        "name",
        "id",
    ]


class PDFInformationAdminForm(forms.ModelForm):

    class Meta:
        model = models.PDFInformation
        fields = "__all__"


class PDFInformationAdmin(admin.ModelAdmin):
    form = PDFInformationAdminForm
    list_display = [
        "file_path",
        "type",
        "file_name",
        "id",
        "created",
        "last_updated",
        "file_size",
    ]
    readonly_fields = [
        "file_path",
        "type",
        "file_name",
        "id",
        "created",
        "last_updated",
        "file_size",
    ]


class RevolvingAccountsDetailsAdminForm(forms.ModelForm):

    class Meta:
        model = models.RevolvingAccountsDetails
        fields = "__all__"


class RevolvingAccountsDetailsAdmin(admin.ModelAdmin):
    form = RevolvingAccountsDetailsAdminForm
    list_display = [
        "account_rating",
        "last_payment",
        "created",
        "payment_status",
        "creditor_remarks",
        "date_opened",
        "high_balance",
        "closed_date",
        "date_of_last_activity",
        "account_number",
        "date_reported",
        "balance_owed",
        "account_status",
        "payment_amount",
        "creditor_type",
        "dispute_status",
        "last_verified",
        "last_updated",
        "id",
        "account_description",
        "term_length",
    ]
    readonly_fields = [
        "account_rating",
        "last_payment",
        "created",
        "payment_status",
        "creditor_remarks",
        "date_opened",
        "high_balance",
        "closed_date",
        "date_of_last_activity",
        "account_number",
        "date_reported",
        "balance_owed",
        "account_status",
        "payment_amount",
        "creditor_type",
        "dispute_status",
        "last_verified",
        "last_updated",
        "id",
        "account_description",
        "term_length",
    ]


admin.site.register(models.BureauScores, BureauScoresAdmin)
admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.PDFInformation, PDFInformationAdmin)
admin.site.register(models.RevolvingAccountsDetails, RevolvingAccountsDetailsAdmin)
