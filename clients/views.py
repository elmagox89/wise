from django.views import generic
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponseBadRequest

from clients.utils import (
    group_texts_near_x_coordinate,
    associate_credit_report_data_with_identifiers,
    extract_credit_scores,
    extract_revolving_accounts
)

from . import models
from . import forms

import logging

logging.basicConfig(filename='app.log', level=logging.DEBUG)

logger = logging.getLogger(__name__)

import json


class BureauScoresListView(generic.ListView):
    model = models.BureauScores
    form_class = forms.BureauScoresForm


class BureauScoresCreateView(generic.CreateView):
    model = models.BureauScores
    form_class = forms.BureauScoresForm


class BureauScoresDetailView(generic.DetailView):
    model = models.BureauScores
    form_class = forms.BureauScoresForm


class BureauScoresUpdateView(generic.UpdateView):
    model = models.BureauScores
    form_class = forms.BureauScoresForm
    pk_url_kwarg = "pk"


class BureauScoresDeleteView(generic.DeleteView):
    model = models.BureauScores
    success_url = reverse_lazy("clients_BureauScores_list")


class ClientListView(generic.ListView):
    model = models.Client
    form_class = forms.ClientForm


class ClientCreateView(generic.CreateView):
    model = models.Client
    form_class = forms.ClientForm


class ClientDetailView(generic.DetailView):
    model = models.Client
    form_class = forms.ClientForm


class ClientUpdateView(generic.UpdateView):
    model = models.Client
    form_class = forms.ClientForm
    pk_url_kwarg = "pk"


class ClientDeleteView(generic.DeleteView):
    model = models.Client
    success_url = reverse_lazy("clients_Client_list")


class PDFInformationListView(generic.ListView):
    model = models.PDFInformation
    form_class = forms.PDFInformationForm


class PDFInformationCreateView(generic.CreateView):
    model = models.PDFInformation
    form_class = forms.UploadPDFForm

    def form_valid(self, form):
        client_id = self.kwargs.get('client_id')
        try:
            client = models.Client.objects.get(id=client_id)
        except models.Client.DoesNotExist:
            return HttpResponseBadRequest("Invalid client ID.")

        pdf_info = form.save(commit=False)
        pdf_info.client_id = client.id
        pdf_info.save()

        pdf_path = pdf_info.file_path.path

        scores = extract_credit_scores(pdf_path)

        bureau_scores = models.BureauScores(
            pdf_information=pdf_info,
            experian_score=scores.get('experian', 0),
            equifax_score=scores.get('equifax', 0),
            transunion_score=scores.get('transunion', 0)
        )

        bureau_scores.save()

        accounts = extract_revolving_accounts(pdf_path)

        revolving_accounts_list = []
        for account in accounts:
            revolving_account = models.RevolvingAccounts(
                account_name=account,
                pdf_data=pdf_info
            )
            revolving_account.save()
            revolving_accounts_list.append(revolving_account)

        column_transunion_groups = group_texts_near_x_coordinate(pdf_path, 174.3280792236328, keyword="Transunion")
        column_experian_groups = group_texts_near_x_coordinate(pdf_path, 308.6957702636719, keyword="Experian")
        column_equifax_groups = group_texts_near_x_coordinate(pdf_path, 443.0634460449219, keyword="Equifax")

        associated_transunion_data = associate_credit_report_data_with_identifiers(column_transunion_groups)
        associated_experian_data = associate_credit_report_data_with_identifiers(column_experian_groups)
        associated_equifax_data = associate_credit_report_data_with_identifiers(column_equifax_groups)

        for i, ra in enumerate(revolving_accounts_list):
            if i < len(associated_transunion_data):
                associated_transunion_data[i]['revolving_account_id'] = ra.id
                self.save_account_details(associated_transunion_data[i], pdf_info)

            if i < len(associated_experian_data):
                associated_experian_data[i]['revolving_account_id'] = ra.id
                self.save_account_details(associated_experian_data[i], pdf_info)

            if i < len(associated_equifax_data):
                associated_equifax_data[i]['revolving_account_id'] = ra.id
                self.save_account_details(associated_equifax_data[i], pdf_info)

        return HttpResponseRedirect(self.get_success_url())

    def save_account_details(self, account_data, pdf_info):
        try:
            account_detail = models.RevolvingAccountsDetails(pdf_data=pdf_info, **account_data)
            account_detail.save()
        except Exception as e:
            error_info = {
                "error": str(e)
            }
            with open('error_logs.json', 'a') as f:
                json.dump(error_info, f)
                f.write('\n')

    def get_success_url(self):
        client_id = self.kwargs.get('client_id')
        return reverse_lazy('clients_Client_detail', kwargs={'pk': client_id})


class PDFInformationDetailView(generic.DetailView):
    model = models.PDFInformation
    form_class = forms.PDFInformationForm


class PDFInformationUpdateView(generic.UpdateView):
    model = models.PDFInformation
    form_class = forms.PDFInformationForm
    pk_url_kwarg = "pk"


class PDFInformationDeleteView(generic.DeleteView):
    model = models.PDFInformation

    def get_success_url(self):
        client_id = self.object.client_id
        return reverse_lazy('clients_Client_detail', kwargs={'pk': client_id})


class RevolvingAccountsDetailsListView(generic.ListView):
    model = models.RevolvingAccountsDetails
    form_class = forms.RevolvingAccountsDetailsForm


class RevolvingAccountsDetailsCreateView(generic.CreateView):
    model = models.RevolvingAccountsDetails
    form_class = forms.RevolvingAccountsDetailsForm


class RevolvingAccountsDetailsDetailView(generic.DetailView):
    model = models.RevolvingAccountsDetails
    form_class = forms.RevolvingAccountsDetailsForm


class RevolvingAccountsDetailsUpdateView(generic.UpdateView):
    model = models.RevolvingAccountsDetails
    form_class = forms.RevolvingAccountsDetailsForm
    pk_url_kwarg = "pk"


class RevolvingAccountsDetailsDeleteView(generic.DeleteView):
    model = models.RevolvingAccountsDetails
    success_url = reverse_lazy("clients_RevolvingAccountsDetails_list")
