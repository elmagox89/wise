from django import forms
from . import models
import fitz



class BureauScoresForm(forms.ModelForm):
    class Meta:
        model = models.BureauScores
        fields = [
            "experian_score",
            "equifax_score",
            "transunion_score",
        ]


class ClientForm(forms.ModelForm):
    class Meta:
        model = models.Client
        fields = [
            "billing_address",
            "employer_name",
            "social_security_number",
            "email",
            "birthdate",
            "residential_address",
            "phone_number",
            "name",
        ]


class PDFInformationForm(forms.ModelForm):
    class Meta:
        model = models.PDFInformation
        fields = [
            "file_path",
            "type",
            "file_name",
            "file_size",
            "client",
        ]

    def __init__(self, *args, **kwargs):
        super(PDFInformationForm, self).__init__(*args, **kwargs)
        self.fields["client"].queryset = models.Client.objects.all()

class UploadPDFForm(forms.ModelForm):
    class Meta:
        model = models.PDFInformation
        fields = ['file_path']

    def save(self, commit=True):
        pdf_file = self.cleaned_data['file_path']
        file_name = pdf_file.name
        file_type = file_name.split('.')[-1]
        file_size = pdf_file.size

        pdf_info = models.PDFInformation(
            file_path=pdf_file,
            type=file_type,
            file_name=file_name,
            file_size=file_size,
        )
        if commit:
            pdf_info.save()
        return pdf_info

class RevolvingAccountsDetailsForm(forms.ModelForm):
    class Meta:
        model = models.RevolvingAccountsDetails
        fields = [
            "account_rating",
            "last_payment",
            "payment_status",
            "creditor_remarks",
            "date_opened",
            "high_balance",
            "closed_date",
            "date_of_last_activity",
            "account_number",
            "date_reported",
            "balance_owed",
            "account_status",
            "payment_amount",
            "creditor_type",
            "dispute_status",
            "last_verified",
            "account_description",
            "term_length",
            "pdf_data",
        ]

    def __init__(self, *args, **kwargs):
        super(RevolvingAccountsDetailsForm, self).__init__(*args, **kwargs)
        self.fields["pdf_data_id"].queryset = models.PDFInformation.objects.all()

