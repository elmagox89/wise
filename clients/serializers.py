from rest_framework import serializers

from . import models


class BureauScoresSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.BureauScores
        fields = [
            "id",
            "experian_score",
            "equifax_score",
            "transunion_score",
            "created",
            "last_updated",
        ]

class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Client
        fields = [
            "created",
            "billing_address",
            "employer_name",
            "social_security_number",
            "last_updated",
            "email",
            "birthdate",
            "residential_address",
            "phone_number",
            "name",
            "id",
        ]

class PDFInformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.PDFInformation
        fields = [
            "file_path",
            "type",
            "file_name",
            "id",
            "created",
            "last_updated",
            "file_size",
            "client_id",
        ]

class RevolvingAccountsDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.RevolvingAccountsDetails
        fields = [
            "account_rating",
            "last_payment",
            "created",
            "payment_status",
            "creditor_remarks",
            "date_opened",
            "high_balance",
            "closed_date",
            "date_of_last_activity",
            "account_number",
            "date_reported",
            "balance_owed",
            "account_status",
            "payment_amount",
            "creditor_type",
            "dispute_status",
            "last_verified",
            "last_updated",
            "id",
            "account_description",
            "term_length",
            "pdf_data_id",
        ]
