from django.urls import path, include
from rest_framework import routers

from . import api
from . import views
from . import htmx


router = routers.DefaultRouter()
router.register("BureauScores", api.BureauScoresViewSet)
router.register("Client", api.ClientViewSet)
router.register("PDFInformation", api.PDFInformationViewSet)
router.register("RevolvingAccountsDetails", api.RevolvingAccountsDetailsViewSet)

urlpatterns = (
    path("api/v1/", include(router.urls)),
    path("clients/BureauScores/", views.BureauScoresListView.as_view(), name="clients_BureauScores_list"),
    path("clients/BureauScores/create/", views.BureauScoresCreateView.as_view(), name="clients_BureauScores_create"),
    path("clients/BureauScores/detail/<uuid:pk>/", views.BureauScoresDetailView.as_view(), name="clients_BureauScores_detail"),
    path("clients/BureauScores/update/<uuid:pk>/", views.BureauScoresUpdateView.as_view(), name="clients_BureauScores_update"),
    path("clients/BureauScores/delete/<uuid:pk>/", views.BureauScoresDeleteView.as_view(), name="clients_BureauScores_delete"),

    path("clients/Client/", views.ClientListView.as_view(), name="clients_Client_list"),
    path("clients/Client/create/", views.ClientCreateView.as_view(), name="clients_Client_create"),
    path("clients/Client/detail/<uuid:pk>/", views.ClientDetailView.as_view(), name="clients_Client_detail"),
    path("clients/Client/update/<uuid:pk>/", views.ClientUpdateView.as_view(), name="clients_Client_update"),
    path("clients/Client/delete/<uuid:pk>/", views.ClientDeleteView.as_view(), name="clients_Client_delete"),

    path("clients/PDFInformation/", views.PDFInformationListView.as_view(), name="clients_PDFInformation_list"),
    path('clients/PDFInformation/create/<uuid:client_id>/', views.PDFInformationCreateView.as_view(), name='clients_PDFInformation_create'),
    path("clients/PDFInformation/detail/<uuid:pk>/", views.PDFInformationDetailView.as_view(), name="clients_PDFInformation_detail"),
    path("clients/PDFInformation/update/<uuid:pk>/", views.PDFInformationUpdateView.as_view(), name="clients_PDFInformation_update"),
    path("clients/PDFInformation/delete/<uuid:pk>/", views.PDFInformationDeleteView.as_view(), name="clients_PDFInformation_delete"),

    path("clients/RevolvingAccountsDetails/", views.RevolvingAccountsDetailsListView.as_view(), name="clients_RevolvingAccountsDetails_list"),
    path("clients/RevolvingAccountsDetails/create/", views.RevolvingAccountsDetailsCreateView.as_view(), name="clients_RevolvingAccountsDetails_create"),
    path("clients/RevolvingAccountsDetails/detail/<uuid:pk>/", views.RevolvingAccountsDetailsDetailView.as_view(), name="clients_RevolvingAccountsDetails_detail"),
    path("clients/RevolvingAccountsDetails/update/<uuid:pk>/", views.RevolvingAccountsDetailsUpdateView.as_view(), name="clients_RevolvingAccountsDetails_update"),
    path("clients/RevolvingAccountsDetails/delete/<uuid:pk>/", views.RevolvingAccountsDetailsDeleteView.as_view(), name="clients_RevolvingAccountsDetails_delete"),

    path("clients/htmx/BureauScores/", htmx.HTMXBureauScoresListView.as_view(), name="clients_BureauScores_htmx_list"),
    path("clients/htmx/BureauScores/create/", htmx.HTMXBureauScoresCreateView.as_view(), name="clients_BureauScores_htmx_create"),
    path("clients/htmx/BureauScores/delete/<uuid:pk>/", htmx.HTMXBureauScoresDeleteView.as_view(), name="clients_BureauScores_htmx_delete"),
    path("clients/htmx/Client/", htmx.HTMXClientListView.as_view(), name="clients_Client_htmx_list"),
    path("clients/htmx/Client/create/", htmx.HTMXClientCreateView.as_view(), name="clients_Client_htmx_create"),
    path("clients/htmx/Client/delete/<uuid:pk>/", htmx.HTMXClientDeleteView.as_view(), name="clients_Client_htmx_delete"),
    path("clients/htmx/PDFInformation/", htmx.HTMXPDFInformationListView.as_view(), name="clients_PDFInformation_htmx_list"),
    path("clients/htmx/PDFInformation/create/", htmx.HTMXPDFInformationCreateView.as_view(), name="clients_PDFInformation_htmx_create"),
    path("clients/htmx/PDFInformation/delete/<uuid:pk>/", htmx.HTMXPDFInformationDeleteView.as_view(), name="clients_PDFInformation_htmx_delete"),
    path("clients/htmx/RevolvingAccountsDetails/", htmx.HTMXRevolvingAccountsDetailsListView.as_view(), name="clients_RevolvingAccountsDetails_htmx_list"),
    path("clients/htmx/RevolvingAccountsDetails/create/", htmx.HTMXRevolvingAccountsDetailsCreateView.as_view(), name="clients_RevolvingAccountsDetails_htmx_create"),
    path("clients/htmx/RevolvingAccountsDetails/delete/<uuid:pk>/", htmx.HTMXRevolvingAccountsDetailsDeleteView.as_view(), name="clients_RevolvingAccountsDetails_htmx_delete"),
)
