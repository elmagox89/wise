import re
from datetime import datetime, timedelta
import fitz


TRANSUNION_X = 174.3280792236328
EXPERIAN_X = 308.6957702636719
EQUIFAX_X = 443.0634460449219

TABLE_ROW_IDENTIFIERS = [
    "Account #", "High Balance:", "Last Verified:",
    "Date of Last Activity:", "Date Reported:", "Date Opened:",
    "Balance Owed:", "Closed Date:",
    "Account Rating:", "Account Description:", "Dispute Status:", "Creditor Type:",
    "Account Status:", "Payment Status:", "Creditor Remarks:", "Payment Amount:",
    "Last Payment:", "Term Length:", "Past Due Amount:", "Account Type:",
    "Payment Frequency:", "Credit Limit:"
]
EXCLUDE_SIZES = [
    6.727727890014648, 7.849016189575195, 18.875015258789062, 14.158130645751953,
    10.465354919433594, 14.950506210327148
]
EXCLUDE_TEXTS = [
    "Partnered", "with", "OK", "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
    "Nov", "Dec", "'22", "'23", "'24", "Show"
]

def extract_credit_scores(pdf_path):
    with fitz.open(pdf_path) as doc:
        page = doc.load_page(0)
        pdf_content = page.get_text()
        content_lines = pdf_content.split('\n')[:20]
        content = '\n'.join(content_lines)

        pattern = r'(Transunion|Experian|Equifax)\s*(\d{3})'
        matches = re.findall(pattern, content)

        scores = {}
        for match in matches:
            agency, score = match
            scores[agency.lower()] = int(score)

    return scores


def extract_name(pdf_path, name):
    with fitz.open(pdf_path) as doc:
        page = doc.load_page(0)
        pdf_content = page.get_text()
        content_one_line = ' '.join(pdf_content.upper().splitlines())

        name_parts = name.upper().split()
        name_pattern = rf'\b{" ".join(name_parts)}'
        for i in range(len(name_parts), 4):
            name_pattern += r'(?:\s+\w+)?'

        name_match = re.search(name_pattern, content_one_line)

        if name_match:
            name_found = ' '.join(name_match.group().split())
            return name_found

    return None

def extract_birth_year(pdf_content):
    # Calcular la fecha válida para el año de cumpleaños
    valid_birth_date = datetime.now() - timedelta(days=21*365)
    valid_birth_year = valid_birth_date.year

    # Expresión regular para encontrar la fecha de cumpleaños en el rango válido
    pattern = rf'\b(19[0-9][0-9]|20[0-{valid_birth_year - 2000}])\b'

    # Tomar las primeras 50 líneas del contenido del PDF
    lines = pdf_content.split('\n')[:50]

    # Convertir las líneas en una sola cadena para buscar el año de cumpleaños
    content_subset = '\n'.join(lines)

    # Buscar el año de cumpleaños en las primeras 50 líneas del contenido del PDF
    birth_year_match = re.search(pattern, content_subset)

    if birth_year_match:
        birth_year = int(birth_year_match.group(0))
        return birth_year

    return None

def to_snake_case(text):
    text = re.sub(r"[^a-zA-Z0-9]", " ", text)
    text = text.lower()
    text = re.sub(r"\s+", "_", text)
    return text.strip("_")

def should_exclude_span_by_text(span):
    """Returns True if the span's text should be excluded based on predefined criteria."""
    return span['text'] in EXCLUDE_TEXTS

def should_exclude_span_by_size(span):
    """Returns True if the span's size should be excluded based on predefined criteria."""
    return span['size'] in EXCLUDE_SIZES

def extract_pdf_text_details(pdf_path):
    """Extracts text details from a PDF while filtering out unwanted text and sizes."""
    pdf_text_details = []

    with fitz.open(pdf_path) as pdf:
        for page in pdf:
            blocks = page.get_text("dict")["blocks"]
            for block in blocks:
                if block["type"] == 0:
                    for line in block["lines"]:
                        for span in line["spans"]:
                            if not should_exclude_span_by_size(span) and not should_exclude_span_by_text(span):
                                pdf_text_details.append({
                                    'text': span['text'],
                                    'size': span['size'],
                                    'bbox': span['bbox'],
                                })

    return pdf_text_details


def extract_revolving_accounts(pdf_path):
    revolving_accounts = []
    with fitz.open(pdf_path) as pdf:
        for page_num in range(len(pdf)):
            page = pdf.load_page(page_num)
            blocks = page.get_text("dict")["blocks"]

            for block in blocks:
                if block["type"] == 0:
                    for line in block["lines"]:
                        for span in line["spans"]:
                            if span['size'] == 11.960405349731445:
                                revolving_accounts.append(span['text'])

    return revolving_accounts


def group_texts_near_x_coordinate(pdf_path, target_x, tolerance=10, keyword=None, expected_group_size=22):
    grouped_texts = []
    current_group = []

    with fitz.open(pdf_path) as pdf:
        for page in pdf:
            blocks = page.get_text("dict")["blocks"]
            for block in blocks:
                if block["type"] != 0:
                    continue

                for line in block["lines"]:
                    for span in line["spans"]:
                        x0 = span['bbox'][0]
                        if should_exclude_span_by_size(span) or should_exclude_span_by_text(span) or not (target_x - tolerance <= x0 <= target_x + tolerance):
                            continue

                        text = span['text']
                        if keyword and text == keyword and current_group:
                            extra_fields_count = len(current_group) - (expected_group_size + 1)  # +1 por el título
                            if 1 <= extra_fields_count <= 3:  # Si hay entre 23 y 25 campos en total
                                merge_start_index = 15  # Comenzar la fusión en la posición 16
                                merge_end_index = 15 + extra_fields_count  # Determinar hasta qué campo fusionar
                                merged_fields = ' '.join(current_group[merge_start_index:merge_end_index + 1])
                                current_group = current_group[:merge_start_index] + [merged_fields] + current_group[merge_end_index + 1:]

                            if len(current_group) == expected_group_size + 1:
                                grouped_texts.append(current_group)
                            current_group = []
                        current_group.append(text)

    if current_group:
        extra_fields_count = len(current_group) - (expected_group_size + 1)
        if 1 <= extra_fields_count <= 3:
            merge_start_index = 15
            merge_end_index = 15 + extra_fields_count
            merged_fields = ' '.join(current_group[merge_start_index:merge_end_index + 1])
            current_group = current_group[:merge_start_index] + [merged_fields] + current_group[merge_end_index + 1:]
        if len(current_group) == expected_group_size + 1:
            grouped_texts.append(current_group)

    return grouped_texts


def associate_credit_report_data_with_identifiers(credit_report_data):
    associated_data = []
    identifiers_snake_case = [to_snake_case(identifier) for identifier in TABLE_ROW_IDENTIFIERS]
    for report in credit_report_data:
        report_dict = {"bureau": report[0]}
        report_values = report[1:]

        if len(report_values) == len(identifiers_snake_case):
            report_dict.update({identifiers_snake_case[i]: report_values[i] for i in range(len(identifiers_snake_case))})
        else:
            print(
                f"Advertencia: Un reporte tiene una cantidad de elementos ({len(report_values)}) diferente a la esperada ({len(identifiers_snake_case)}).")
        associated_data.append(report_dict)
    return associated_data
