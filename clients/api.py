from rest_framework import viewsets, permissions

from . import serializers
from . import models


class BureauScoresViewSet(viewsets.ModelViewSet):
    """ViewSet for the BureauScores class"""

    queryset = models.BureauScores.objects.all()
    serializer_class = serializers.BureauScoresSerializer
    permission_classes = [permissions.IsAuthenticated]


class ClientViewSet(viewsets.ModelViewSet):
    """ViewSet for the Client class"""

    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer
    permission_classes = [permissions.IsAuthenticated]


class PDFInformationViewSet(viewsets.ModelViewSet):
    """ViewSet for the PDFInformation class"""

    queryset = models.PDFInformation.objects.all()
    serializer_class = serializers.PDFInformationSerializer
    permission_classes = [permissions.IsAuthenticated]


class RevolvingAccountsDetailsViewSet(viewsets.ModelViewSet):
    """ViewSet for the RevolvingAccountsDetails class"""

    queryset = models.RevolvingAccountsDetails.objects.all()
    serializer_class = serializers.RevolvingAccountsDetailsSerializer
    permission_classes = [permissions.IsAuthenticated]
