import os
import re
from django.db import models
from django.urls import reverse
import uuid

class BureauScores(models.Model):

    pdf_information = models.ForeignKey("clients.PDFInformation", on_delete=models.CASCADE)
    # Fields
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    experian_score = models.SmallIntegerField()
    equifax_score = models.SmallIntegerField()
    transunion_score = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        pass

    def __str__(self):
        return str(self.pk)

    def get_absolute_url(self):
        return reverse("clients_BureauScores_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("clients_BureauScores_update", args=(self.pk,))

    @staticmethod
    def get_htmx_create_url():
        return reverse("clients_BureauScores_htmx_create")

    def get_htmx_delete_url(self):
        return reverse("clients_BureauScores_htmx_delete", args=(self.pk,))


class Client(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    billing_address = models.TextField(max_length=200, blank=True)
    employer_name = models.TextField(max_length=200, blank=True)
    social_security_number = models.TextField(max_length=200, blank=True)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    email = models.EmailField(blank=True)
    birthdate = models.IntegerField(null=True, blank=True)
    residential_address = models.TextField(max_length=200, blank=True)
    phone_number = models.PositiveIntegerField(null=True, blank=True)
    name = models.TextField(max_length=100, blank=False)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        pass

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("clients_Client_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("clients_Client_update", args=(self.pk,))

    @staticmethod
    def get_htmx_create_url():
        return reverse("clients_Client_htmx_create")

    def get_htmx_delete_url(self):
        return reverse("clients_Client_htmx_delete", args=(self.pk,))


class PDFInformation(models.Model):

    # Relationships
    client = models.ForeignKey("clients.Client", on_delete=models.CASCADE)

    # Fields
    content = models.TextField()
    file_path = models.FileField(upload_to="upload/pdfs/")
    type = models.TextField(max_length=100)
    file_name = models.TextField()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    file_size = models.PositiveIntegerField()

    def delete(self, *args, **kwargs):
        if self.file_path:
            if os.path.isfile(self.file_path.path):
                os.remove(self.file_path.path)

        super(PDFInformation, self).delete(*args, **kwargs)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return str(self.pk)

    def get_absolute_url(self):
        return reverse("clients_PDFInformation_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("clients_PDFInformation_update", args=(self.pk,))

    @staticmethod
    def get_htmx_create_url():
        return reverse("clients_PDFInformation_htmx_create")

    def get_htmx_delete_url(self):
        return reverse("clients_PDFInformation_htmx_delete", args=(self.pk,))

class RevolvingAccounts(models.Model):
    pdf_data = models.ForeignKey("clients.PDFInformation", on_delete=models.CASCADE)

    account_name = models.TextField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        ordering = ['account_name']

    def __str__(self):
        return f"{self.bureau} - {self.account_number}"

class RevolvingAccountsDetails(models.Model):

    # Relationships
    pdf_data = models.ForeignKey("clients.PDFInformation", on_delete=models.CASCADE)
    revolving_account = models.ForeignKey("clients.RevolvingAccounts", on_delete=models.CASCADE)

    # Fields
    bureau = models.TextField(max_length=100, null=True, blank=True)
    account_rating = models.TextField(max_length=100, null=True, blank=True)
    account = models.TextField(max_length=100, null=True, blank=True)
    past_due_amount = models.TextField(max_length=100, null=True, blank=True)
    account_type = models.TextField(max_length=100, null=True, blank=True)
    payment_frequency = models.TextField(max_length=100, null=True, blank=True)
    last_payment = models.TextField(null=True, blank=True)  # Can be null
    created = models.DateTimeField(auto_now_add=True, editable=False)
    payment_status = models.TextField(max_length=100, null=True, blank=True)
    creditor_remarks = models.TextField(max_length=100, null=True, blank=True)
    date_opened = models.TextField(null=True, blank=True)  # Can be null
    high_balance = models.TextField(max_length=100, null=True, blank=True)
    closed_date = models.TextField(null=True, blank=True)  # Can be null
    date_of_last_activity = models.TextField(null=True, blank=True)  # Can be null
    account_number = models.TextField(max_length=100, null=True, blank=True)
    credit_limit = models.TextField(max_length=100, null=True, blank=True)
    date_reported = models.TextField(null=True, blank=True)  # Can be null
    balance_owed = models.IntegerField(null=True, blank=True)
    account_status = models.TextField(max_length=100, null=True, blank=True)
    payment_amount = models.IntegerField(null=True, blank=True)
    creditor_type = models.TextField(max_length=100, null=True, blank=True)
    dispute_status = models.TextField(max_length=100, null=True, blank=True)
    last_verified = models.TextField(max_length=100, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    account_description = models.TextField(max_length=100, null=True, blank=True)
    term_length = models.TextField(max_length=100, null=True, blank=True)



    class Meta:
        pass


    def __str__(self):
        return str(self.pk)

    def clean(self):
        integer_fields = ['balance_owed', 'payment_amount', 'past_due_amount', 'high_balance', 'credit_limit']
        for field_name in integer_fields:
            value = getattr(self, field_name, None)
            if isinstance(value, str):
                cleaned_value = re.sub(r'[^\d.]', '', value)
                try:
                    setattr(self, field_name, int(float(cleaned_value)))
                except ValueError:
                    setattr(self, field_name, None)
            elif value == "--":
                setattr(self, field_name, None)


    def save(self, *args, **kwargs):
        self.clean()
        super(RevolvingAccountsDetails, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("clients_RevolvingAccountsDetails_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("clients_RevolvingAccountsDetails_update", args=(self.pk,))

    @staticmethod
    def get_htmx_create_url():
        return reverse("clients_RevolvingAccountsDetails_htmx_create")

    def get_htmx_delete_url(self):
        return reverse("clients_RevolvingAccountsDetails_htmx_delete", args=(self.pk,))
