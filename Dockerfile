FROM python:3.10

# Establece el directorio de trabajo
WORKDIR /wise

# Copia el script wait-for-it.sh al contenedor
COPY wait-for-it.sh /wait-for-it.sh

# Haz que wait-for-it.sh sea ejecutable
RUN chmod +x /wait-for-it.sh

# Copia el archivo de requerimientos primero para aprovechar la caché de Docker
COPY requirements.txt .

# Instala las dependencias de Python
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

# Copia el resto de los archivos de la aplicación al contenedor
COPY . .

# Ejecuta la aplicación
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--access-logfile", "-", "wise.wsgi:application"]