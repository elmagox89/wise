import pytest
import test_helpers

from django.urls import reverse


pytestmark = [pytest.mark.django_db]


def tests_BureauScores_list_view(client):
    instance1 = test_helpers.create_clients_BureauScores()
    instance2 = test_helpers.create_clients_BureauScores()
    url = reverse("clients_BureauScores_list")
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance1) in response.content.decode("utf-8")
    assert str(instance2) in response.content.decode("utf-8")


def tests_BureauScores_create_view(client):
    url = reverse("clients_BureauScores_create")
    data = {
        "experian_score": 1,
        "equifax_score": 1,
        "transunion_score": 1,
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_BureauScores_detail_view(client):
    instance = test_helpers.create_clients_BureauScores()
    url = reverse("clients_BureauScores_detail", args=[instance.pk, ])
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance) in response.content.decode("utf-8")


def tests_BureauScores_update_view(client):
    instance = test_helpers.create_clients_BureauScores()
    url = reverse("clients_BureauScores_update", args=[instance.pk, ])
    data = {
        "experian_score": 1,
        "equifax_score": 1,
        "transunion_score": 1,
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_Client_list_view(client):
    instance1 = test_helpers.create_clients_Client()
    instance2 = test_helpers.create_clients_Client()
    url = reverse("clients_Client_list")
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance1) in response.content.decode("utf-8")
    assert str(instance2) in response.content.decode("utf-8")


def tests_Client_create_view(client):
    url = reverse("clients_Client_create")
    data = {
        "billing_address": "text",
        "employer_name": "text",
        "social_security_number": "text",
        "email": "user@tempurl.com",
        "birthdate": datetime.now(),
        "residential_address": "text",
        "phone_number": 1,
        "name": "text",
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_Client_detail_view(client):
    instance = test_helpers.create_clients_Client()
    url = reverse("clients_Client_detail", args=[instance.pk, ])
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance) in response.content.decode("utf-8")


def tests_Client_update_view(client):
    instance = test_helpers.create_clients_Client()
    url = reverse("clients_Client_update", args=[instance.pk, ])
    data = {
        "billing_address": "text",
        "employer_name": "text",
        "social_security_number": "text",
        "email": "user@tempurl.com",
        "birthdate": datetime.now(),
        "residential_address": "text",
        "phone_number": 1,
        "name": "text",
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_PDFInformation_list_view(client):
    instance1 = test_helpers.create_clients_PDFInformation()
    instance2 = test_helpers.create_clients_PDFInformation()
    url = reverse("clients_PDFInformation_list")
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance1) in response.content.decode("utf-8")
    assert str(instance2) in response.content.decode("utf-8")


def tests_PDFInformation_create_view(client):
    client_id = test_helpers.create_clients_Client()
    url = reverse("clients_PDFInformation_create")
    data = {
        "content": "text",
        "file_path": "aFile",
        "type": "text",
        "file_name": "text",
        "file_size": 1,
        "client_id": client_id.pk,
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_PDFInformation_detail_view(client):
    instance = test_helpers.create_clients_PDFInformation()
    url = reverse("clients_PDFInformation_detail", args=[instance.pk, ])
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance) in response.content.decode("utf-8")


def tests_PDFInformation_update_view(client):
    client_id = test_helpers.create_clients_Client()
    instance = test_helpers.create_clients_PDFInformation()
    url = reverse("clients_PDFInformation_update", args=[instance.pk, ])
    data = {
        "content": "text",
        "file_path": "aFile",
        "type": "text",
        "file_name": "text",
        "file_size": 1,
        "client_id": client_id.pk,
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_RevolvingAccountsDetails_list_view(client):
    instance1 = test_helpers.create_clients_RevolvingAccountsDetails()
    instance2 = test_helpers.create_clients_RevolvingAccountsDetails()
    url = reverse("clients_RevolvingAccountsDetails_list")
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance1) in response.content.decode("utf-8")
    assert str(instance2) in response.content.decode("utf-8")


def tests_RevolvingAccountsDetails_create_view(client):
    pdf_data_id = test_helpers.create_clients_PDFInformation()
    url = reverse("clients_RevolvingAccountsDetails_create")
    data = {
        "account_rating": "text",
        "last_payment": datetime.now(),
        "payment_status": "text",
        "creditor_remarks": "text",
        "date_opened": datetime.now(),
        "high_balance": "text",
        "closed_date": datetime.now(),
        "date_of_last_activity": datetime.now(),
        "account_number": "text",
        "date_reported": datetime.now(),
        "balance_owed": 1,
        "account_status": "text",
        "payment_amount": 1,
        "creditor_type": "text",
        "dispute_status": "text",
        "last_verified": "text",
        "account_description": "text",
        "term_length": "text",
        "pdf_data_id": pdf_data_id.pk,
    }
    response = client.post(url, data)
    assert response.status_code == 302


def tests_RevolvingAccountsDetails_detail_view(client):
    instance = test_helpers.create_clients_RevolvingAccountsDetails()
    url = reverse("clients_RevolvingAccountsDetails_detail", args=[instance.pk, ])
    response = client.get(url)
    assert response.status_code == 200
    assert str(instance) in response.content.decode("utf-8")


def tests_RevolvingAccountsDetails_update_view(client):
    pdf_data_id = test_helpers.create_clients_PDFInformation()
    instance = test_helpers.create_clients_RevolvingAccountsDetails()
    url = reverse("clients_RevolvingAccountsDetails_update", args=[instance.pk, ])
    data = {
        "account_rating": "text",
        "last_payment": datetime.now(),
        "payment_status": "text",
        "creditor_remarks": "text",
        "date_opened": datetime.now(),
        "high_balance": "text",
        "closed_date": datetime.now(),
        "date_of_last_activity": datetime.now(),
        "account_number": "text",
        "date_reported": datetime.now(),
        "balance_owed": 1,
        "account_status": "text",
        "payment_amount": 1,
        "creditor_type": "text",
        "dispute_status": "text",
        "last_verified": "text",
        "account_description": "text",
        "term_length": "text",
        "pdf_data_id": pdf_data_id.pk,
    }
    response = client.post(url, data)
    assert response.status_code == 302
