import fitz  # Importa PyMuPDF

from tabulate import tabulate

pdf_path = 'judy.pdf'
output_pdf_path = '3-Bureau_Credit_Report__Scores_3-5-2024__SmartCredit_highlighted.pdf'  # Define el path del archivo de salida


# Inicializa la lista para guardar los detalles
pdf_details = []

# Define los criterios de exclusión
exclude_sizes = [6.727727890014648, 7.849016189575195, 18.875015258789062, 14.158130645751953, 10.465354919433594,
                 14.950506210327148]

exclude_texts = ["Partnered", "with", "OK", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
                 "Dec", "'22", "'23", "'24", "Show"]

# Lista para cuentas revolving
revolving_accounts = []


# Usamos una lista por si hay múltiples textos que cumplen la condición
first_column_texts = []

allowed_texts = [
    "Account #",
    "High Balance:",
    "Last Verified:",
    "Date of Last Activity:",
    "Date Reported:",
    "Date Opened:",
    "Balance Owed:",
    "Closed Date:",
    "Account Rating:",
    "Account Description:",
    "Dispute Status:",
    "Creditor Type:",
    "Account Status:",
    "Payment Status:",
    "Creditor Remarks:",
    "Payment Amount:",
    "Last Payment:",
    "Term Length:",
    "Past Due Amount:",
    "Account Type:",
    "Payment Frequency:",
    "Credit Limit:"
]



specific_value = 39.96040344238281
# Verifica si el span debe ser excluido
def should_exclude(span):
    if span['size'] in exclude_sizes or span['text'] in exclude_texts:
        return True
    if any(value == specific_value for value in span['bbox']) and span['text'] in allowed_texts:
        first_column_texts.append({'text':span['text'], 'bbox':span['bbox']})
        return True
    return False


# Abre el documento PDF
with fitz.open(pdf_path) as pdf:
    # Itera a través de las páginas del documento
    for page_num in range(len(pdf)):
        page = pdf.load_page(page_num)

        # Extrae los bloques de texto de la página
        blocks = page.get_text("dict")["blocks"]

        for block in blocks:
            if block["type"] == 0:  # Asegúrate de que es un bloque de texto
                for line in block["lines"]:
                    for span in line["spans"]:
                        if should_exclude(span):
                            continue

                        if span['size'] == 11.960405349731445:
                            revolving_accounts.append({span['text']})

                        pdf_details.append({
                            'text': span['text'],
                            'size': span['size'],
                            'bbox': span['bbox'],
                        })

def encontrar_valores_columna(first_column_texts, revolving_account_details, num_columna, tolerancia_y = 1):

    columna_valores = []
    for first_text in first_column_texts:
        x_referencia = first_text['bbox'][2]
        y_primera_col = (first_text['bbox'][1] + first_text['bbox'][3]) / 2
        valores_encontrados = []

        for detail in revolving_account_details:
            x_izquierda = detail['bbox'][0]
            y_detail = (detail['bbox'][1] + detail['bbox'][3]) / 2
            if x_izquierda > x_referencia and abs(y_detail - y_primera_col) < tolerancia_y:
                valores_encontrados.append(detail)

        if len(valores_encontrados) >= num_columna:
            detalle_seleccionado = valores_encontrados[num_columna - 1]  # Accede al detalle deseado
            texto_detalle = detalle_seleccionado['text']  # Extrae el texto
            bbox_detalle = detalle_seleccionado['bbox']  # Extrae las coordenadas del bbox
            columna_valores.append(f"{texto_detalle}|{bbox_detalle}")
        else:
            columna_valores.append('#####')

    return columna_valores

valores_segunda_columna = encontrar_valores_columna(
    first_column_texts,
    pdf_details,
    2,
    1
)
valores_tercera_columna = encontrar_valores_columna(
    first_column_texts,
    pdf_details,
    3,
)
valores_cuarta_columna = encontrar_valores_columna(
    first_column_texts,
    pdf_details,
    4,
)


filas = []
for i, texto in enumerate(allowed_texts):
    fila = [texto,
            valores_segunda_columna[i] if i < len(valores_segunda_columna) else '',
            valores_tercera_columna[i] if i < len(valores_tercera_columna) else '',
            valores_cuarta_columna[i] if i < len(valores_cuarta_columna) else '']
    filas.append(fila)

# Imprimir la tabla
print(tabulate(filas, headers=['', 'Transunion', 'Experian', 'Equifax']))