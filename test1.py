import fitz
import json
import re

# Constants
TRANSUNION_X = 174.3280792236328
EXPERIAN_X = 308.6957702636719
EQUIFAX_X = 443.0634460449219

revolving_accounts = []

#PDF_PATH = 'juan.pdf'
PDF_PATH = 'judy.pdf'

TABLE_ROW_IDENTIFIERS = [
    "Account #", "High Balance:", "Last Verified:", "Date of Last Activity:",
    "Date Reported:", "Date Opened:", "Balance Owed:", "Closed Date:",
    "Account Rating:", "Account Description:", "Dispute Status:", "Creditor Type:",
    "Account Status:", "Payment Status:", "Creditor Remarks:", "Payment Amount:",
    "Last Payment:", "Term Length:", "Past Due Amount:", "Account Type:",
    "Payment Frequency:", "Credit Limit:"
]
EXCLUDE_SIZES = [
    6.727727890014648, 7.849016189575195, 18.875015258789062, 14.158130645751953,
    10.465354919433594, 14.950506210327148
]
EXCLUDE_TEXTS = [
    "Partnered", "with", "OK", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct", "Nov", "Dec", "'22", "'23", "'24", "Show"
]

def to_snake_case(text):
    text = re.sub(r"[^a-zA-Z0-9]", " ", text)
    text = text.lower()
    text = re.sub(r"\s+", "_", text)
    return text.strip("_")

def should_exclude_span_by_text(span):
    return span['text'] in EXCLUDE_TEXTS

def should_exclude_span_by_size(span):
    return span['size'] in EXCLUDE_SIZES

def extract_pdf_text_details(pdf_path):
    pdf_text_details = []
    with fitz.open(pdf_path) as pdf:
        for page in pdf:
            blocks = page.get_text("dict")["blocks"]
            for block in blocks:
                if block["type"] == 0:
                    for line in block["lines"]:
                        for span in line["spans"]:
                            if not should_exclude_span_by_size(span) and not should_exclude_span_by_text(span):
                                pdf_text_details.append({
                                    'text': span['text'],
                                    'size': span['size'],
                                    'bbox': span['bbox'],
                                })
    return pdf_text_details

def find_column_positions(page, identifiers, initial_x_values, initial_tolerance=1, max_tolerance=6, increment=0.5):
    column_positions = {'TRANSUNION': None, 'EXPERIAN': None, 'EQUIFAX': None}
    for identifier in identifiers:
        text_instances = page.search_for(identifier)
        for inst in text_instances:
            bbox = inst
            for column_name, initial_x in initial_x_values.items():
                tolerance = initial_tolerance
                found = False
                while tolerance <= max_tolerance and not found:
                    search_area = (bbox[2] + tolerance, bbox[1], bbox[2] + tolerance + 5, bbox[3])
                    words_in_area = page.get_text("words", clip=search_area)
                    for word in words_in_area:
                        word_bbox = word[:4]
                        if initial_x - tolerance <= word_bbox[0] <= initial_x + tolerance:
                            column_positions[column_name] = word_bbox[0]
                            found = True
                            break
                    tolerance += increment
    return column_positions

pdf_text_details = extract_pdf_text_details(PDF_PATH)

# Example of how to use find_column_positions with a specific page
# This is a placeholder example, as the actual implementation will vary depending on your document structure
# and how you wish to iterate through the pages and use the positions found.
initial_x_values = {
    'TRANSUNION': TRANSUNION_X,
    'EXPERIAN': EXPERIAN_X,
    'EQUIFAX': EQUIFAX_X,
}

# Assuming usage in a context where we iterate through pages or a specific page is targeted
with fitz.open(PDF_PATH) as pdf:
    for page_num, page in enumerate(pdf):
        column_positions = find_column_positions(page, TABLE_ROW_IDENTIFIERS, initial_x_values)
        print(f"Column positions for page {page_num}: {column_positions}")

pdf_details = extract_pdf_text_details(PDF_PATH)

for info in pdf_details:
    print(info)